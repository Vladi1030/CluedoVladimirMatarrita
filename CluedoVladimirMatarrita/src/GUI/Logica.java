/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import cluedovladimirmatarrita.*;
import java.util.LinkedList;
import javax.swing.JLabel;

/**
 *
 * @author Vladimir
 */
public class Logica {

    private LinkedList<Usuario> usuarios;
    private EditorArchivo editar;
    private LinkedList<Partida> juego;
    private LinkedList<Carta> temp;
    private String codActual;
    LinkedList<String> crimen;
    int jugActual;

    public Logica() {
        usuarios = new LinkedList<>();
        editar = new EditorArchivo();
        crimen = new LinkedList<>();
        juego = new LinkedList<>();
        temp = new LinkedList<>();
    }

    /**
     *
     * @param usuario (Usuario con los datos necesarios) Agrega los datos de un
     * usuario a un archivo de texto
     */
    public void agregarUsuario(Usuario usuario) {
        usuarios.add(usuario);
        String texto = editar.leerTextoArchivo("Usuarios.txt");
        editar.escribirTextoArchivo("Usuarios.txt", texto + procesarUsuario(usuario));
    }

    /**
     *
     * @param u(Usuario al que se le procesarán los datos) procesa los datos y
     * los acomoda en un String para guardarlos en el archivo
     * @return un String con todos los datos del usuario
     */
    public String procesarUsuario(Usuario u) {
        String datosUsuario = "";
        datosUsuario += u.getLogIn() + ";";
        datosUsuario += u.getContrasenna() + ";";
        datosUsuario += u instanceof UsuarioConEmail
                ? ((UsuarioConEmail) u).getEmail() + ";" : ";";
        datosUsuario += u.getNombre().getNombre() + ";";
        datosUsuario += u.getNombre().getApellidoPaterno() + ";";
        datosUsuario += u.getNombre() instanceof NombreLatino
                ? ((NombreLatino) u.getNombre()).getNombreMaterno() + ";" : ";";
        datosUsuario += u.getTipo() + ";";
        datosUsuario += u.getSexo();
        return datosUsuario;
    }

    /**
     *
     * @param c
     * @param archivo
     */
    public void cartasCrimen(String archivo) {
        String texto = editar.leerTextoArchivo(archivo);
        editar.escribirTextoArchivo(archivo, "Cartas del crimen: " + crimen.get(0) + ";"
                + crimen.get(1) + ";" + crimen.get(2) + "\n" + texto);
    }

    /**
     * Agrega los usuarios que están en el archivo de texto a un LinkedList
     */
    public void cargarUsuarios() {
        String texto = editar.leerTextoArchivo("Usuarios.txt");
        if (!texto.isEmpty()) {
            Usuario u = new Usuario();
            String[] usuario = texto.split("\n");
            for (int i = 0; i < usuario.length; i++) {
                String[] datosUsuario = usuario[i].split(";");
                if (!datosUsuario[2].trim().isEmpty()) {
                    u = new UsuarioConEmail();
                    ((UsuarioConEmail) u).setEmail(datosUsuario[2].trim());
                }
                u.setContrasenna(datosUsuario[1].trim());
                u.setLogIn(datosUsuario[0].trim());
                u.setTipo(TipoDeUsuario.valueOf(datosUsuario[6]));
                Nombre n = new Nombre();
                if (!datosUsuario[5].trim().isEmpty()) {
                    n = new NombreLatino();
                    ((NombreLatino) n).setNombreMaterno(datosUsuario[5].trim());
                }
                n.setNombre(datosUsuario[3].trim());
                n.setApellidoPaterno(datosUsuario[4].trim());
                u.setNombre(n);
                u.setSexo(datosUsuario[7].trim());
                usuarios.add(u);
            }
        }
    }

    /**
     * reparte todas las cartas entr los usuarios
     */
    public void repartirCartas() {
        boolean puede = true;
        crimen.clear();
        int ale;
        Crimen c = new Crimen();
        LinkedList<String> cartasFinales = new LinkedList<>();
        String[] cartas = editar.leerTextoArchivo("Cartas.txt").split("\n");
        for (int i = 0; i < cartas.length; i++) {
            cartasFinales.add(cartas[i]);
        }
        for (int i = 0; i < 3; i++) {
            Carta ca = new Carta();
            if (i == 0) {
                ale = ale(5, 0);
                crimen.add(cartas[ale]);
            } else if (i == 1) {
                ale = ale(9, 6);
                crimen.add(cartas[ale]);
            } else {
                ale = ale(16, 12);
                crimen.add(cartas[ale]);
            }
            cartasFinales.remove(ale);
        }
        cartasCrimen(codActual + ".txt");
        //---------------------------------------------------------bien
        String[] texto = editar.leerTextoArchivo(codActual + ".txt").split("\n");
        int j = 1;
        do {
            ale = ale((cartasFinales.size() - 1), 0);
            if (j == texto.length) {
                j = 1;
            }
            texto[j] += ";" + cartasFinales.get(ale);
            cartasFinales.remove(ale);
            j++;
        } while (cartasFinales.size() > 0);
        String textoF = "";
        for (int i = 0; i < texto.length; i++) {
            if (i == texto.length - 1) {
                textoF += texto[i];
            } else {
                textoF += texto[i] + "\n";
            }
        }
        editar.escribirTextoArchivo(codActual + ".txt", textoF);
    }

    /**
     * verifica a ver si lo que que usuarios dice está entre las cartas de los usuarios
     * @param sugerencia (cartas que el usuario sugiere)
     */
    public void sugerir(String[] sugerencia) {
        String[] texto = editar.leerTextoArchivo(codActual + ".txt").split("\n");
        String fin = "";
        for (int i = 0; i < texto.length-1; i++) {
            String[] dato = texto[i].split(";");
            if (dato[0].equals("Sugirió") || dato[0].equals("Acusó")) {

            } else {
                fin += texto[i] + "\n";
            }
        }
        fin += "Sugirió" +";" + sugerencia[0] + ";" + sugerencia[1] + ";" + sugerencia[2] + "\n";
        fin += texto[texto.length - 1];
        editar.escribirTextoArchivo(codActual + ".txt", fin);
    }

    /**
     * 
     * @param acusado(cartas que el usuario  acusa)
     * recorre las cartas del crimen a ver si el usuario tiene razón o no
     * @return si tiene razón o no
     */
    public boolean acusar(String[] acusado) {
        crimen.clear();
        String[] texto = editar.leerTextoArchivo(codActual + ".txt").split("\n");
        String car = texto[0];
        String replace = car.replaceAll("Cartas del crimen: ", "");
        String carCri[] = replace.split(";");
        for (int i = 0; i < carCri.length; i++) {
            crimen.add(carCri[i]);
        }
        if (((acusado[0].equals(crimen.get(0))) || (acusado[0].equals(crimen.get(1))) || (acusado[0].equals(crimen.get(2))))
                && ((acusado[1].equals(crimen.get(0))) || (acusado[0].equals(crimen.get(1))) || (acusado[0].equals(crimen.get(2))))
                && ((acusado[2].equals(crimen.get(0))) || (acusado[2].equals(crimen.get(1))) || (acusado[2].equals(crimen.get(2))))) {
            return true;
        }
        return false;
    }

    /**
     * 
     * @param car(carta)
     * verifica si la carta la tiene un usuario
     * @return si está o no
     */
    public boolean verificarCartas(String car){
        String []texto=editar.leerTextoArchivo(codActual+".txt").split("\n");
        String []cartas=texto[0].split(";");
        for (int i = 0; i < cartas.length; i++) {
            if(i==0){
                if(!(cartas[i].replace("Cartas del crimen: ", "").equals(car))){
                    return true;
                }
            }else{
                if(!(cartas[i].equals(car))){
                    return true;
                }
            }
        }
        return false;
    }
    
    /**
     * cambia de turno de participante
     */
    public void cambiarTurno() {
        jugActual++;
        String[] texto = editar.leerTextoArchivo(codActual + ".txt").split("\n");
        if (jugActual == texto.length - 3) {
            jugActual = 1;
        }
        String fin = "";
        for (int i = 0; i < texto.length; i++) {
            if (i == texto.length - 1) {
                String dat[] = texto[jugActual].split(";");
                fin += dat[0];
            } else {
                fin += texto[i] + "\n";
            }
        }
        editar.escribirTextoArchivo(codActual + ".txt", fin);
    }

    /**
     * crea un aleatorio
     * @param max
     * @param min
     * @return  el aleatorio
     */
    public int ale(int max, int min) {
        return (int) (Math.random() * (max - min) + min);
    }

    /**
     * verifica el usuario al que le toca jugar
     * @param cod
     * @param nombre
     * @return 
     */
    public boolean turno(String cod, String nombre) {
        String[] texto = editar.leerTextoArchivo(cod + ".txt").split("\n");
        if (texto[texto.length - 1].equals(nombre)) {
            return true;
        }
        return false;
    }

    /**
     * obtiene las cartas de un jugador
     * @param cod
     * @param nombre
     * @return 
     */
    public LinkedList<String> obtenerMisCartas(String cod, String nombre) {
        LinkedList<String> cartas = new LinkedList<>();
        String texto = editar.leerTextoArchivo(cod + ".txt");
        String[] datos = texto.split("\n");
        for (int i = 0; i < datos.length; i++) {
            String[] cards = datos[i].split(";");
            if (cards[0].equals(nombre)) {
                for (int j = 1; j < cards.length; j++) {
                    cartas.add(cards[i]);
                }
            }
        }
        return cartas;
    }

    /**
     * decide quien comienza
     */
    public void decidirOrden() {
        String[] texto = editar.leerTextoArchivo(codActual + ".txt").split("\n");
        int cant = 0;
        for (int i = 1; i < texto.length; i++) {
            String[] cartas = texto[i].split(";");
            if (cant < (cartas.length - 2)) {
                cant = cartas.length;
                jugActual = i;
            }
        }
        String fin = "";
        for (int i = 0; i < texto.length; i++) {
            if (i == texto.length - 1) {
                String dat[] = texto[i].split(";");
                fin += texto[i] + "\n";
                fin += dat[0];
            } else {
                fin += texto[i] + "\n";
            }
        }
        editar.escribirTextoArchivo(codActual + ".txt", fin);
    }

    /**
     *
     * @param cod
     * @return
     */
    public boolean juegoListo(String cod, String nombre) {
        String texto = editar.leerTextoArchivo(cod + ".txt");
        String[] dat = texto.split("\n");
        String[] ultima = dat[dat.length - 1].split(";");
        System.out.println(ultima.length);
        if (ultima.length == 1) {
            return true;
        }
        return false;
    }

    /**
     *
     * @param password(contraseña del usuario)
     * @param login(Nombre de usuario) recorre un LinkedList para verificar si
     * un usuario existe
     * @return los datos del usuario
     */
    public Usuario verificarLogin(String password, String login) {
        for (int i = 0; i < usuarios.size(); i++) {
            if (usuarios.get(i).getLogIn().equals(login)
                    || ((UsuarioConEmail) usuarios.get(i)).getEmail().equals(login)) {
                if (usuarios.get(i).getContrasenna().equals(password)) {
                    return usuarios.get(i);
                }
            }
        }
        return null;
    }

    /**
     *
     * @param login(Nombre de usuario)
     * @param correo(Correo del usuario) recorre los usuarios y verifica que no
     * existan los datos en otro usuario
     * @return un String que permite saber si ya existe el correo o el usuario
     */
    public String permitirRegistro(String login, String correo) {
        for (int i = 0; i < usuarios.size(); i++) {
            if (usuarios.get(i).getLogIn().trim().equals(login.trim())) {
                return "El nombre de usuario ya está en uso";
            } else if (((UsuarioConEmail) usuarios.get(i)).getEmail().trim().equals(correo.trim())) {
                return "El correo se encuentra en uso";
            }
        }
        return "";
    }

    /**
     *
     * @return
     */
    public int crearPartida(Usuario u) {
        int ale = (int) (Math.random() * 99999 + 1);
        String texto = editar.leerTextoArchivo("Partidas.txt");
        editar.escribirTextoArchivo(ale + ".txt", u.getNombre().getNombre());
        editar.escribirTextoArchivo("Partidas.txt", texto + ale + ";" + u.getNombre().getNombre());
        return ale;
    }

    /**
     *
     * @param nombre
     * @param archivo
     */
    public void unirse(String nombre, String archivo) {
        String texto = editar.leerTextoArchivo("Partidas.txt");
        String[] par = texto.split("\n");
        String guardar = "";
        for (int i = 0; i < par.length; i++) {
            String[] datosPar = par[i].split(";");
            if (i == (par.length - 1)) {
                if ((archivo).equals((datosPar[0]))) {
                    guardar += par[i] + ";" + nombre;
                } else {
                    guardar += par[i];
                }
            } else if ((archivo).equals((datosPar[0]))) {
                guardar += par[i] + ";" + nombre + "\n";
            } else {
                guardar += par[i] + "\n";
            }
        }
        editar.escribirTextoArchivo("Partidas.txt", guardar);
        String dat = editar.leerTextoArchivo(archivo + ".txt");
        editar.escribirTextoArchivo(archivo + ".txt", dat + nombre);
    }

    public boolean verificar(String cod) {
        String texto = editar.leerTextoArchivo(codActual + ".txt");
        String[] dat = texto.split("\n");
        if (dat.length > 2) {
            return true;
        }
        return false;
    }

    public LinkedList<Carta> getTemp() {
        return temp;
    }

    public void setTemp(Carta temp) {
        this.temp.add(temp);
    }

    public LinkedList<Usuario> getUsuarios() {
        return usuarios;
    }

    public void setUsuarios(Usuario usuarios) {
        this.usuarios.add(usuarios);
    }

    public LinkedList<Partida> getJuego() {
        return juego;
    }

    public String getCodActual() {
        return codActual;
    }

    public void setCodActual(String codActual) {
        this.codActual = codActual;
    }
}
