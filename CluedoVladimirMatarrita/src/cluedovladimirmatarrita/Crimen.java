/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cluedovladimirmatarrita;

/**
 *
 * @author Vladimir
 */
public class Crimen {
    private Carta arma;
    private Carta lugar;
    private Carta persona;

    public Crimen(Carta arma, Carta lugar, Carta persona) {
        this.arma = arma;
        this.lugar = lugar;
        this.persona = persona;
    }

    public Crimen() {
    }

    public Carta getArma() {
        return arma;
    }

    public void setArma(Carta arma) {
        this.arma = arma;
    }

    public Carta getLugar() {
        return lugar;
    }

    public void setLugar(Carta lugar) {
        this.lugar = lugar;
    }

    public Carta getPersona() {
        return persona;
    }

    public void setPersona(Carta persona) {
        this.persona = persona;
    }
    
    
}
