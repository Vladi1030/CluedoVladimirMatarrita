/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cluedovladimirmatarrita;

import java.util.LinkedList;

/**
 *
 * @author Vladimir
 */
public class Partida {
    private String nombre;
    private LinkedList<String> jugadores;

    public Partida() {
    }

    public Partida(String nombre, String jugadores, int cantJuga) {
        this.nombre = nombre;
        this.jugadores.add(jugadores);
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public LinkedList<String> getJugadores() {
        return jugadores;
    }

    public void setJugadores(LinkedList<String> jugadores) {
        this.jugadores = jugadores;
    }
    
    @Override
    public String toString() {
        return "Partida{" + "nombre=" + nombre + ", jugadores=" + jugadores + '}';
    }
    
}
