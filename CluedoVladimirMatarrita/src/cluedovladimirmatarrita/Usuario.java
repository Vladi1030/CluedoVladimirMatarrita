/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cluedovladimirmatarrita;

/**
 *
 * @author Vladimir
 */
public class Usuario {
    private Nombre Nombre;
    private String logIn;
    private String contrasenna;
    private TipoDeUsuario tipo;
    private String sexo;

    public String getSexo() {
        return sexo;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    public Nombre getNombre() {
        return Nombre;
    }

    public void setNombre(Nombre Nombre) {
        this.Nombre = Nombre;
    }

    public String getLogIn() {
        return logIn;
    }

    public void setLogIn(String logIn) {
        this.logIn = logIn;
    }

    public String getContrasenna() {
        return contrasenna;
    }

    public void setContrasenna(String contrasenna) {
        this.contrasenna = contrasenna;
    }

    public TipoDeUsuario getTipo() {
        return tipo;
    }

    public void setTipo(TipoDeUsuario tipo) {
        this.tipo = tipo;
    }

    @Override
    public String toString() {
        return "Usuario{" + "Nombre=" + Nombre + ", logIn=" + logIn + ", contrasenna=" + contrasenna + ", tipo=" + tipo + '}';
    }
    
    
}
