/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cluedovladimirmatarrita;

/**
 *
 * @author Vladimir
 */
public class UsuarioConEmail extends Usuario{
    private String email;

    public UsuarioConEmail() {
    }

    public UsuarioConEmail(String email) {
        this.email = email;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String toString() {
        return "UsuarioConEmail{" + "email=" + email + '}';
    }
    
}
