/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cluedovladimirmatarrita;

/**
 *
 * @author Vladimir
 */
public class Nombre {
    private String nombre;
    private String apellidoPaterno;

    public Nombre(String nombre, String apellidoPaterno) {
        this.nombre = nombre;
        this.apellidoPaterno = apellidoPaterno;
    }

    public Nombre() {
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellidoPaterno() {
        return apellidoPaterno;
    }

    public void setApellidoPaterno(String apellidoPaterno) {
        this.apellidoPaterno = apellidoPaterno;
    }

    @Override
    public String toString() {
        return "Nombre{" + nombre + " " + apellidoPaterno + "}";
    }
    
}
