/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cluedovladimirmatarrita;

/**
 *
 * @author Vladimir
 */
public class Sobre {
    private Carta arma;
    private Carta personaje;
    private Carta lugar;

    public Sobre(Carta personaje, Carta lugar) {
        this.personaje = personaje;
        this.lugar = lugar;
    }

    public Sobre() {
    }

    public Carta getArma() {
        return arma;
    }

    public void setArma(Carta arma) {
        this.arma = arma;
    }

    public Carta getPersonaje() {
        return personaje;
    }

    public void setPersonaje(Carta personaje) {
        this.personaje = personaje;
    }

    public Carta getLugar() {
        return lugar;
    }

    public void setLugar(Carta lugar) {
        this.lugar = lugar;
    }
}
