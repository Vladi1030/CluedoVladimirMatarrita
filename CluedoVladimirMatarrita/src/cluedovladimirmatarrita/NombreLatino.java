/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cluedovladimirmatarrita;

/**
 *
 * @author Vladimir
 */
public class NombreLatino extends Nombre{
    
    private String apellidoMaterno;

    public NombreLatino(String nombreMaterno, String nombre, String apellidoPaterno) {
        super(nombre, apellidoPaterno);
        this.apellidoMaterno = nombreMaterno;
    }

    public NombreLatino() {
    }

    public String getNombreMaterno() {
        return apellidoMaterno;
    }

    public void setNombreMaterno(String nombreMaterno) {
        this.apellidoMaterno = nombreMaterno;
    }

    @Override
    public String toString() {
        return "NombreLatino{" + "nombreMaterno=" + apellidoMaterno + '}';
    }
    
}
